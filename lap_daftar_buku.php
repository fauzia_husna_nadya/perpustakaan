<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>
	
	<script>
		function printContent(el){
			var restorepage = document.body.innerHTML;
			var printcontent = document.getElementById(el).innerHTML;
			document.body.innerHTML = printcontent;
			window.print();
			document.body.innerHTML = restorepage;
		}
	</script>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Sistem Informasi Perpustakaan</a>
            </div>
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['user_id'];?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="home.php"><i class="glyphicon glyphicon-home"></i> Home </a>
                    </li>
                    <li>
                        <a href="daftar_buku.php"><i class="fa fa-book"></i> Buku</a>
                    </li>
                    <li>
                        <a href="data_siswa.php"><i class="fa fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="data_peminjaman.php"><i class="fa fa-shopping-cart"></i> Peminjaman</a>
                    </li>
                    <li>
                        <a href="data_pengembalian.php"><i class="fa fa-reply"></i> Pengembalian</a>
                    </li>
                    <li>
                        <a href="lap_daftar_buku.php"><i class="fa fa-file-text"></i> Laporan</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Laporan Daftar Buku
                        </h1>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
								<div id="div1">
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th> No </th>
												<th> Kode Buku </th>
												<th> Nama Buku </th>
                                                <th> Nama Penerbit </th>
                                                <th> Nama Pengarang </th>
                                                <th> Jumlah Buku </th>
                                                <th> Tanggal Terima </th>
                                                <th> Tanggal Entry </th>
											</tr>
										</thead>
										<?php
											include "koneksi.php";
												$tampil = "SELECT * FROM table_buku order by nama_buku;";
												$hasil = mysql_query($tampil);
												$no = 1;
											while ($row=mysql_fetch_array($hasil))
											{
												echo "<tr>";
												echo "<td> $no </td>";
												echo "<td> $row[kode_buku] </td>";
												echo "<td> $row[nama_buku] </td>";
                                                echo "<td> $row[penerbit] </td>";
                                                echo "<td> $row[pengarang] </td>";
                                                echo "<td> $row[jumlah_buku] </td>";
                                                echo "<td> $row[tgl_terima] </td>";
                                                echo "<td> $row[tgl_entry] </td>";
												echo "</tr>";
															$no++;
											}
										?>
									</table>
								</div>
                                    <form class="navbar-form navbar-left" action="#">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default">Laporan Data Siswa</button>
                                        </div>
                                    </form>
                                    <form class="navbar-form navbar-left" action="#">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default">Laporan Pengembalian Buku</button>
                                        </div>
                                    </form>
									<a href="#">
										<i class="fa fa-print fa-4x" onclick="printContent('div1')" style="float:right;" title="Print"></i>
									<a>
							</div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
