<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Edit Data Pengembalian Buku </title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Sistem Informasi Perpustakaan</a>
            </div>
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['user_id'];?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="home.php"><i class="glyphicon glyphicon-home"></i> Home </a>
                    </li>
                    <li>
                        <a href="daftar_buku.php"><i class="fa fa-book"></i> Buku</a>
                    </li>
                    <li>
                        <a href="data_siswa.php"><i class="fa fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="data_peminjaman.php"><i class="fa fa-shopping-cart"></i> Peminjaman</a>
                    </li>
                    <li>
                        <a href="data_pengembalian.php"><i class="fa fa-reply"></i> Pengembalian</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-file-text"></i> Laporan</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit Data Pengembalian Buku
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    	<?php
                    		include "koneksi.php";
                    		$edit=mysql_query("SELECT * FROM table_pengembalian");
                    		$row=mysql_fetch_array($edit);
	                    ?>
	                        <form method='POST' action='proses_edit_pengembalian.php'>
	                            <label>Tanggal Pinjam</label>
                                    <input class='form-control' name='tgl_pinjam' type='date' placeholder='Tahun/Bulan/Tanggal' value="<?php echo $row['tgl_pinjam'] ?>" readonly required>
                                <label>Tanggal Kembali</label>
                                    <input class='form-control' name='tgl_kembali' type='date' placeholder='Tahun/Bulan/Tanggal' value="<?php echo $row['tgl_kembali'] ?>" readonly required>
                                <label>Kode Buku</label>
									<input class='form-control' name='kode_buku' type='text' value="<?php echo $row['kode_buku'] ?>" readonly required>
		                        <label>Nama Buku</label>
									<input class='form-control' name='nama_buku' type='text' value="<?php echo $row['nama_buku'] ?>" readonly required>
								<label>NIS</label>
									<input class='form-control' name='nis' type='text' value="<?php echo $row['nis'] ?>" readonly required>
								<label>Nama Siswa</label>
									<input class='form-control' name='nama_siswa' type='text' value="<?php echo $row['nama_siswa'] ?>" readonly required>
								<div class="form-group">
                                <label>Tanggal Dikembalikan</label>
                                	<input class='form-control' name='tgl_dikembalikan' type='date' placeholder='Tahun/Bulan/Tanggal' value="<?php echo $row['tgl_dikembalikan'] ?>" required>
								</div>
                                    <button type='submit' class='btn btn-default' value='Update'>Simpan</button>
	                        </form>
                    </div>                 
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
