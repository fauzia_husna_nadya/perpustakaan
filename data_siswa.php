<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Data Siswa </title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Sistem Informasi Perpustakaan</a>
            </div>
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['user_id'];?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="home.php"><i class="glyphicon glyphicon-home"></i> Home </a>
                    </li>
                    <li>
                        <a href="daftar_buku.php"><i class="fa fa-book"></i> Buku</a>
                    </li>
                    <li>
                        <a href="data_siswa.php"><i class="fa fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="data_peminjaman.php"><i class="fa fa-shopping-cart"></i> Peminjaman</a>
                    </li>
                    <li>
                        <a href="data_pengembalian.php"><i class="fa fa-reply"></i> Pengembalian</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-file-text"></i> Laporan</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Data Siswa
                        </h1>
                    </div>
                </div>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <form class="navbar-form navbar-right" action="tambah_siswa.php">
                        <?php if($_SESSION['user_id']=='1'){?>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">+  Tambah Siswa</button>
                        </div>
                        <?php }?>
                    </form>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th> No </th>
                                            <th> NIS </th>
                                            <th> Nama Siswa </th>
											<th> Jenis Kelamin </th>
                                            <th> Kelas </th>
                                            <th> Jurusan </th>
                                        <?php if($_SESSION['user_id']=='1'){?>
                                            <th> Aksi </th>
                                        <?php } ?>    
                                        </tr>
                                    </thead>
                                        <?php
                                        include "koneksi.php";
                                            $tampil = mysql_query("SELECT * FROM table_siswa order by nama_siswa;");
                                            $no = 1;
                                        while ($row=mysql_fetch_array($tampil))
                                        {
                                            echo '<tr>';
                                            echo '<td>' .$no. '</td>';
                                            echo '<td>' .$row['nis']. '</td>';
                                            echo '<td>' .$row['nama_siswa']. '</td>';
                                            echo '<td>' .$row['jenis_kelamin']. '</td>';
                                            echo '<td>' .$row['kelas']. '</td>';
                                            echo '<td>' .$row['jurusan']. '</td>';
                                        if($_SESSION['user_id']=='1'){
                                            echo '<td><a href="edit_siswa.php?id='.$row['nis'].'" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></i></a>
                                                        <a href="hapus_siswa.php?id='.$row['nis'].'" onclick="return confirm(\'Anda Yakin Menghapusnya?\')" title="Hapus"><i class="fa fa-trash-o fa-2x"></i></a>
                                                        </td>';
                                        }
                                            echo '</tr>';
                                                        $no++;
                                        }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>