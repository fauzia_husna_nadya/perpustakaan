<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Pinjam Buku </title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Sistem Informasi Perpustakaan</a>
            </div>
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['user_id'];?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="home.php"><i class="glyphicon glyphicon-home"></i> Home </a>
                    </li>
                    <li>
                        <a href="daftar_buku.php"><i class="fa fa-book"></i> Buku</a>
                    </li>
                    <li>
                        <a href="data_siswa.php"><i class="fa fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="data_peminjaman.php"><i class="fa fa-shopping-cart"></i> Peminjaman</a>
                    </li>
                    <li>
                        <a href="data_pengembalian.php"><i class="fa fa-reply"></i> Pengembalian</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-file-text"></i> Laporan</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Pinjam Buku
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    <?php
                            include "koneksi.php";
                            mysql_select_db("table_peminjaman");
                                $q=mysql_query("SELECT kode_pinjam FROM table_peminjaman ORDER BY kode_pinjam DESC LIMIT 1");
                                $datakode=mysql_fetch_array($q);
                                $kode=substr($datakode['kode_pinjam'],4,3);
                                    $tambah=$kode+1;
                                    
                                if($tambah<10)
                                {
                                    $kode_pinjam="PJ0000".$tambah;
                                }else{
                                    $kode_pinjam="PJ000".$tambah;
                                }
                        ?>
                        <form method="POST" action="simpan_peminjaman.php">
                            <label>Kode Pinjam</label>
                                <input class="form-control" name="kode_pinjam" type="text" value="<?php echo $kode_pinjam;?>" readonly required>
                            <label>Kode Buku</label>
                                <input class="form-control" name="kode_buku" type="text" required placeholder="Kode Buku">
                            <label>Nama Buku</label>
                                <input class="form-control" name="nama_buku" type="text" required placeholder="Nama Buku">
                            <label>NIS</label>
                                <input class="form-control" name="nis" type="text" required placeholder="NIS">
                            <label>Nama Siswa</label>
                                <input class="form-control" name="nama_siswa" type="text" required placeholder="Nama Siswa">
                            <label>Kelas</label>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <select class="form-control" name="kelas">
                                            <option value="X"> X </option>
                                            <option value="XI"> XI </option>
                                            <option value="XII"> XII </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-9">
                                        <input class="form-control" name="jurusan" type="text" required>
                                    </div>
                                </div>
                            <label>Tanggal Pinjam</label>
                                <input class="form-control" name="tgl_pinjam" type="date" required>
                            <div class="form-group">
                                <label>Tanggal Kembali</label>
                                <input class="form-control" name="tgl_kembali" type="date" required>
                            </div>
                            <button type="submit" class="btn btn-default">Simpan</button>
                            <button type="reset" class="btn btn-default">Batal</button>
                        </form>
                    </div>                 
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
